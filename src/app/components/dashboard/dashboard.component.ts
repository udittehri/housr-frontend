import { ProductsService } from 'src/app/services/products.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private productService: ProductsService, private http: HttpClient) { }
  baseUrl: String = 'http://localhost:5000/api/' // We can store it in config file but for time i am just writting it here 
  products: any = []
  newProduct = {
    name: '',
    description: '',
    type: '99',
    frequency: '99',
    basePrice: Number,
    sellPrice: Number,
    subsPrice: Number,
    subsBasePrice: Number,
    gst: Number
  }
  selectedProduct : any
  error: any

  ngOnInit() {
    this.getProducts();
  }

  selectProduct(item)
  {
    delete item.__v;
    delete item._id;
    delete item.created_at;
    delete item.updated_at;
    this.selectedProduct = item;
  }

  async getProducts() {
    this.http.get(`${this.baseUrl}product`)
      .subscribe(
        (res) => {
          this.products = (res)
          console.log(this.products);
        }
      )
  }
  async deleteProducts(id, status) {
    let criteria = {
      id: id,
      change: {
        status: status == 1 ? '2' : '1'
      }
    }
    // We can make a seprate file for services and get POST Patch request and we can use it from there 
    //  but for now it is in this same file only. Just for example purpose. 
    this.http.patch(`${this.baseUrl}product`, criteria)
      .subscribe(
        (res) => {
          console.log(res);
          this.getProducts();
        }
        , error => {
          // if (error.error.e.message) this.error = error.error.e.message
          console.error('onRejected function called: ' + error.error.e);
        })
  }

  async addProduct() {
    this.http.post(`${this.baseUrl}product`, this.newProduct)
      .subscribe(
        (res) => {
          this.getProducts();
        }, error => {
          if (error.error.e.message) this.error = error.error.e.message
          console.error('onRejected function called: ' + error.error.e.message);
        })
  }


}
