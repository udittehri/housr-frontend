import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'assignment-Angular';
  local = true;
  dataSet: any = [{
    image: 'ABC.png',
    name: 'Mango',
    description: 'It is fruit',
    price: 120,
    Dprice: 110
  }, {
    image: 'ABC.png',
    name: 'Mango',
    description: 'It is fruit',
    price: 120,
    Dprice: 110
  }];

  temp = {
    image: '',
    name: '',
    description: '',
    price: 0,
    Dprice: 0,
  };
  isAdd = false;
  isEdit = false;

  isAddToggle(value, edx, index) {
    // console.log(value, edx);
    // True for Add 
    if (value) {
      this.isAdd = true
    }
    else {
      this.isEdit = true;
      this.temp = edx;
      // this.dataSet.splice (index, 1 )
      this.deleteRow(index);
    }

  }
  deleteRow(index) {
    this.dataSet.splice(index, 1)
  }

  cancelEdit() {
    this.temp = {
      image: '',
      name: '',
      description: '',
      price: 0,
      Dprice: 0,
    };
    this.isAdd = false;  // Just for the check 
    this.isEdit = false;
  }

  update() {
    let URL = ""
    if (this.isAdd)
      URL = "/add"
    else
      URL = '/edit'

    // This is for local data 
    if (this.local) {
      // if (this.isAdd )
      // {
      this.dataSet.push(this.temp)
      // }  
    }

    this.cancelEdit();
  }

}
