import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpClientModule } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  baseUrl : String = 'http://localhost:5000/api/' // We can store it in config file but for time i am just writting it here 


  constructor(private http : HttpClient) { }

  getProducts () {
    let r = {};
    this.http.get(`${this.baseUrl}product`)
    .subscribe(
      (res)=>{
        r = {...res};

      }
    )
    return r ;
  }
  addProducts () {
    return this.http.get(`${this.baseUrl}/products`)
    .subscribe(
      (res)=>{

      }
    )
  }

  changeProduct () {
    return this.http.get(`${this.baseUrl}/products`)
    .subscribe(
      (res)=>{

      }
    )
  }
}

